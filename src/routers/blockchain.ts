import { Router } from 'express';
import { 
  getBlockChainHeight, 
  getBlock, 
  getBlocks, 
  getSequence, 
  getTotalTransaction, 
  getAllTransaction, 
  getBalance, 
  commitTransaction, 
  getEnergy} from '../api/blockchain-api';
import { SHA256 } from 'crypto-js';
import { equals, isEmpty, compose, flatten } from 'ramda';
import Database from '../db';
import { getRange, createAccountTransactions, paymentTransactions, postTransactions, updateAccountTransactions } from '../api/utils';
import { BlockInfo, Transaction } from '../types';
import colors from 'colors/safe';
import { IUser, UserModel, PostModel, IPost } from '../db/models';
import { decode, sign, encode } from '../../lib/tx';
import axios from 'axios';
import { readFileSync } from 'fs';

const router = Router();

router.get('/get_blockchain_height', async (req, res) => {
  const height = await getBlockChainHeight();
  res.setHeader('Content-Type', 'application/json');
  res.send({ height });
});

router.get('/get_block/:height', async (req, res) => {
  const { height } = req.params;
  const block = await getBlock(+height);
  
  res.setHeader('Content-Type', 'application/json');
  return res.send({ block });
});

router.get('/get_blocks/:from/:to', async (req, res) => {
  const { from, to } = req.params;
  const blocks = await getBlocks(+from, +to);
  res.setHeader('Content-Type', 'application/json');
  return res.send({ blocks });
});

router.get('/get_sequence/:publicKey', async (req, res) => {
  const { publicKey } = req.params;
  const sequence = await getSequence(publicKey);
  res.setHeader('Content-Type', 'application/json');
  return res.send({ sequence });
});

router.get('/init_users_database/:pass', async (req, res) => {
  const database = new Database('mongodb://localhost:27017/test');
  if (equals(SHA256(req.params.pass).toString(), 'b2debb05e41b6171beda1631712b6f352d3bd3c9ad7a0af61d153e0f24ec8288')) {
    const ranges = compose(getRange)(await getBlockChainHeight());
    let createAccountBlocks = Array<BlockInfo[]>();
    let paymentBlocks = Array<BlockInfo[]>();
    let postBlocks = Array<BlockInfo[]>();
    let updateAccountBlocks = Array<BlockInfo[]>();
    for (const range of ranges) {
      const { from, to } = range;
      console.log(colors.cyan(`Fetch block range: ${from} --> ${to}`));
  
      const blocks = await getBlocks(from, to);
      createAccountBlocks.push(createAccountTransactions(blocks));
      paymentBlocks.push(paymentTransactions(blocks));
      postBlocks.push(postTransactions(blocks));
      updateAccountBlocks.push(updateAccountTransactions(blocks));
    }

    if (!isEmpty(createAccountBlocks)) {
      const users = flatten<BlockInfo>(createAccountBlocks).map(block => ({
        publicKey: block.txs[0].params.address,
        height: block.height,
        time: block.time,
      }) as IUser);
      try {
        await database.connect();
        await database.addUsers(users);
      } catch (err) {
        res.setHeader('Content-Type', 'application/json');
        return res.send({
          error: 'Internal Server Failed',
          err
        });
      }
      database.close();
    } 

    if (!isEmpty(updateAccountBlocks)) {
      await database.connect();
      const promises = flatten<BlockInfo>(updateAccountBlocks).map(async block => {
        const { params, account } = block.txs[0];
        const { key, value } = params;
        switch (key) {
          case 'name': 
            await UserModel.findOneAndUpdate({publicKey: account}, { displayName: value.toString('utf8')}).exec();
            break;
          
          case 'picture': 
            await UserModel.findOneAndUpdate({publicKey: account}, { image: value}).exec();
            break;
        }
      });
      await Promise.all(promises);
      database.close();
    }

    if (!isEmpty(postBlocks)) {
      await database.connect();

      const promises = flatten<BlockInfo>(postBlocks).map(async block => {
        const { height, time } = block;
        const { params, account } = block.txs[0];
        const { keys, content } = params;
        const post = new PostModel({
          account,
          height,
          time,
          content,
          keys
        } as IPost)
        return await post.save();
      })
      await Promise.all(promises);
      database.close();
    }
  } else {
    res.setHeader('Content-Type', 'application/json');
    return res.send({ response: 'Not Allowed'});
  }

  res.setHeader('Content-Type', 'application/json');
  return res.send({result: 'Fetching done'})

});

router.get('/get_total_transaction/:publicKey', async (req, res) => {
  const { publicKey } = req.params;
  const total_count = await getTotalTransaction(publicKey);
  res.setHeader('Content-Type', 'application/json');
  return res.send({
    total_count
  });
});

router.get('/get_all_transactions/:publicKey', async (req, res) => {
  const { publicKey } = req.params;
  const transactions = await getAllTransaction(publicKey);
  res.setHeader('Content-Type', 'application/json');
  return res.send({
    transactions
  });
});

router.get('/get_balance/:publicKey', async (req, res) => {
  const { publicKey } = req.params;
  const balanceInfo = await getBalance(publicKey);
  res.setHeader('Content-Type', 'application/json');
  res.send(balanceInfo);
});

router.get('/commit_transaction/:transaction', async (req, res) => {
  const { transaction } = req.params;
  const result = await commitTransaction(transaction)
  res.send({ 
    ...result
  });
});

router.get('/create_transaction/:publicKey/:operation', async (req, res) => {
  const { publicKey, operation } = req.params;
  const transaction: Transaction = {
    version: 1,
    account: publicKey,
    sequence: await getSequence(publicKey) + 1,
    operation: operation,
  }
  return res.send({ transaction });
});


export default router;
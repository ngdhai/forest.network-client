import { Router } from 'express';
import { getBlockChainHeight } from '../api/blockchain-api';
import { equals, compose, toString, map, isEmpty, project } from 'ramda';
import { SHA256 } from 'crypto-js';
import mongoose from 'mongoose';
import { 
  getSequence, 
  getAllTransaction, 
  getTransaction, 
  getBalance, 
  getUser, 
  fetch, 
  refetch,
  stopFetch, 
  getPosts, 
  getEnergy, 
  getFollowers, 
  getFollowingsDetail, 
  getFollowingCount,
  getAllUserSimple,
  getNewFeed,
  getPayment
} from '../api/db-api';
import { PASSWORD } from '../constants/forest-network';
import {} from 'ramda';


const router = Router();

router.get('/init/:pass', async (req, res) => {

  res.setHeader('Content-Type', 'application/json');
  const { pass } = req.params;
  const check = compose(
    equals(PASSWORD),
    toString,
    SHA256
  )(pass);

  if (!check) {
    return res.send({result: 'Wrong Password'});
  }

  const currentHeight = await getBlockChainHeight();
  const result = await fetch(currentHeight);

  res.send({
    result
  })
});

router.get('/re_fetch', async (req, res) => {
  refetch();
  res.end();
});

router.get('/stop', async (req, res) => {
  stopFetch();
  res.end();
})

router.get('/open_database_connect', (req, res) => {
  mongoose.connect('mongodb://localhost:27017/test', { useNewUrlParser: true });
  res.send({ })
});

router.get('/close_database_connect', () => {
  mongoose.disconnect();
});

router.get('/get_sequence/:publicKey', async (req, res) => {
  const { publicKey } = req.params;
  const sequence = await getSequence(publicKey);

  res.setHeader('Content-Type', 'application/json');
  res.send({result:{sequence}});
});

router.get('/get_all_transaction/:publicKey', async (req, res) => {
  const { publicKey } = req.params;
  const result = await getAllTransaction(publicKey);
  res.setHeader('Content-Type', 'application/json');
  res.send({result});
});

router.get('/get_transaction/:publicKey', async (req, res) => {
  const { publicKey } = req.params;
  const transactions = await getTransaction(publicKey);
  res.setHeader('Content-Type', 'application/json');
  res.send({result: transactions});
});

router.get('/get_energy/:publicKey', async (req, res) => {
  const { publicKey } = req.params;
  const { energy, time } = await getEnergy(publicKey);
  res.setHeader('Content-Type', 'application/json');
  res.send({
    result: {
      energy,
      time
    }
  })
});

router.get('/get_balance/:publicKey', async (req, res) => {
  const { publicKey } = req.params;
  const balance = await getBalance(publicKey);
  res.setHeader('Content-Type', 'application/json');
  res.send({
    result: {
      balance
    }
  });
});

router.get('/get_posts/:publicKey', async (req, res) => {
  const { publicKey } = req.params;
  const posts = await getPosts(publicKey);
  const data = posts.map(post => ({
    height: post.height,
    time: post.time,
    content: post.params.content.buffer,
    dataHash: post.dataHash,
    keys: post.params.keys,
    account: post.account,
    reacts: post.reacts
  }))
  res.setHeader('Content-Type', 'application/json');
  res.send({result: data})
});

router.get('/get_account_summary/:publicKey', async (req, res) => {
  const { publicKey } = req.params;
  const user = await getUser(publicKey);
  const followingCount = await getFollowingCount(publicKey);
  const { energy } = await getEnergy(publicKey);
  const followers = await getFollowers(publicKey);

  res.setHeader('Content-Type', 'application/json');
  res.send({
    result: {
      publicKey: user.account,
      createdAt: user.time,
      balance: user.balance,
      displayName: user.getDisplayName(),
      createdAtBlock: user.height,
      followingCount,
      energy,
      image: user.image,
      followerCount: followers.length
    }
  });
});

router.get('/get_user_info/:publicKey', async (req, res) => {
  const { publicKey } = req.params;
  const user = await getUser(publicKey);
  res.setHeader('Content-Type', 'application/json');
  res.send({
    result: {
      publicKey: user.account,
      createdAt: user.time,
      balance: user.balance,
      displayName: user.getDisplayName(),
      image: user.image,
    }
  });
})

router.get('/get_follower/:publicKey', async (req, res) => {
  const { publicKey } = req.params;
  const data = map(follower => {
    return {
      publicKey: follower.account,
      createdAt: follower.time,
      balance: follower.balance,
      displayName: follower.getDisplayName(),
      image: follower.image
    }
  } ,await getFollowers(publicKey)) ;
  res.setHeader('Content-Type', 'application/json');
  res.send({result: data})
})

router.get('/get_following/:publicKey', async (req, res) => {
  const { publicKey } = req.params;
  const data = map(following => {
    return {
      publicKey: following.account,
      time: following.time,
      balance: following.balance,
      displayName: following.displayName,
      image: following.image
    }
  }, await getFollowingsDetail(publicKey));
  res.setHeader('Content-Type', 'application/json');
  res.send({result: data});
})

router.get('/get_all_users/', async (req, res) => {
  const data = await getAllUserSimple();
  res.setHeader('Content-Type', 'application/json');
  res.send({result: data});
})

router.get('/new_feeds/:publicKey', async (req, res) => {
  const { publicKey } = req.params;
  const feeds = await getNewFeed(publicKey);
  const data = !isEmpty(feeds) ? feeds.map(feed => ({
    height: feed.height,
    time: feed.time,
    content: feed.params.content.buffer,
    keys: feed.params.keys,
    account: feed.account,
    reacts: feed.reacts,
    displayName: feed.displayName,
    image: feed.image,
    dataHash: feed.dataHash
  })) : feeds;
  res.setHeader('Content-Type', 'application/json');
  res.send({result: data});
});

router.get('/get_payments/:publicKey', async (req, res) => {
  const { publicKey } = req.params;
  const payments = await getPayment(publicKey);
  const result = project(['account', 'height', 'time', 'params', 'dataHash'], payments);
  res.setHeader('Content-Type', 'application/json');
  res.send({result});
})

export default router;
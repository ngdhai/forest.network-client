import express, { Router } from 'express';
import { decode, sign, encode } from '../../lib/tx';
import axios from 'axios';
import { getSequence } from '../api/db-api';
import { commitTransaction } from '../api/blockchain-api';
import { Transaction } from '../types';
import { API_URL_HTTP } from '../constants/forest-network';
import { contentEncode } from '../encoder';

const router = Router();
router.use(express.urlencoded({ extended: false}));
router.use(express.json())

router.get('/commit/:transaction', async (req, res) => {
  const { transaction } = req.params;
  const result = await commitTransaction(transaction);
  res.setHeader('Content-Type', 'application/json');
  return res.send({
    ...result
  });
});

router.get('/create/:publicKey/:operation', async (req, res) => {
  const { publicKey, operation } = req.params;
  const transaction: Transaction = {
    version: 1,
    account: publicKey,
    sequence: await getSequence(publicKey) + 1,
    operation: operation,
  }
  res.setHeader('Content-Type', 'application/json');
  return res.send({ transaction });
});

router.get('/decode/:tx', async (req, res) => {
  const { tx } = req.params;
  const transaction: Transaction = decode(Buffer.from(tx, 'base64'));
  res.setHeader('Content-Type', 'application/json');
  res.send({
    transaction
  })
});

router.post('/rpc_commit', async (req, res) => {
  const { transaction } = req.body;
  res.setHeader('Content-Type', 'application/json');
  axios.post(API_URL_HTTP, {
    method: 'broadcast_tx_commit',
    jsonrpc: '2.0',
    params: [transaction],
    id: 'dontcare'
  }).then(
    result => res.send({ result })
  ).catch(err => res.send({err}))
});


router.post('/content_encode', async (req, res) => {
  const { content } = req.body;
  const encoded = contentEncode(content);
  
  res.setHeader('Content-Type', 'application/json');
  return res.send({
    result: encoded
  })
});

export default router;

interface Params {
  address?: string,
  amount?: number,
  content?: any,
  keys?: any,
  key?: string,
  value?: Buffer,
  object?: string
}

type Operation = 'create_account' | 'payment' | 'post' | 'update_account' | string;

interface Transaction {
  version: number,
  account: string,
  sequence: number,
  memo?: Buffer,
  operation?: Operation,
  params?: Params,
  signature?: Buffer
}

interface BlockInfo {
  numTxs: Number,
  totalTxs: Number;
  height: Number,
  time: Date,
  txs: Transaction[] | null
}

interface Block {
  numTxs: number
  totalTxs: number
  height: number
  time: Date
  dataHash: string
  txs: Transaction[] | null
  reacts: any[]
}

export {
  Params,
  Operation,
  Transaction,
  BlockInfo,
  Block
}
import express from 'express';
import { blockchainAPI, databaseAPI, commitAPI } from './routers';
import colors from 'colors/safe';
import morgan from 'morgan';
import axios from 'axios';
import { sign, encode } from '../lib/tx';
import { Transaction } from './types';
import { contentEncode, followingEncode } from './encoder';
import { readFileSync } from 'fs';

const app = express();
const logger = morgan('short');
const port = process.env.PORT || 4000;

app.use(logger);
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

app.get('/', (req, res) => {
  res.send('Hello world');
});

app.get('/ex/create_account', async (req, res) => {
  const { data } = await axios.get('http://localhost:4000/commit/v1/create/GB73OPHUZC3RSDEU2LYV5T7MEAN2Q26HYQPDYIENGNBUHW5CXAQ6UJOO/create_account');
  const tx: Transaction = {
    ...data.transaction,
    memo: Buffer.alloc(0),
    params: {
      address: 'GBFAXXKK5BPLRSVHBMR7UMLFXRVYCGYDEABIDFDIBTRDLRWGVODOBIZK'
    },
    signature: Buffer.alloc(64, 0)
  };
  sign(tx, 'SAOPUWKOSPA5BILCP2YTDW2KRWMCBYLBC2EPVXGLUQSPPZV4222CXLET');
  await axios.get(`http://localhost:4000/commit/v1/commit/0x${encode(tx).toString('hex')}`);
  res.setHeader('Content-Type', 'application/json');
  res.send({tx: `0x${encode(tx).toString('hex')}`});
});

app.get('/ex/payment', async (req, res) => {
  const { data } = await axios.get('http://localhost:4000/commit/v1/create/GBUHOVI2MHLTRUGI234VMDNEJDO3DNOX37XOIQFUKBYD5QHAUYG2ETYP/payment');
  const tx: Transaction = {
    ...data.transaction,
    memo: Buffer.alloc(0),
    params: {
      address: 'GB73OPHUZC3RSDEU2LYV5T7MEAN2Q26HYQPDYIENGNBUHW5CXAQ6UJOO',
      amount: 120000000
    },
    signature: Buffer.alloc(64, 0)
  };
  sign(tx, 'SCDYCOAJWXMCWNBMADJ2QN4SXZKXLQRDWMZHFAFRFSRH3NBD3DB4ON4M');
  await axios.get(`http://localhost:4000/commit/v1/commit/0x${encode(tx).toString('hex')}`);
  res.setHeader('Content-Type', 'application/json');
  res.send({tx: `0x${encode(tx).toString('hex')}`});
});

app.get('/ex/post', async (req, res) => {
  const { data } = await axios.get('http://localhost:4000/commit/v1/create/GB73OPHUZC3RSDEU2LYV5T7MEAN2Q26HYQPDYIENGNBUHW5CXAQ6UJOO/post');
  const content = { 
    type: 1, 
    text: `獄中無酒亦無花
    對此良宵奈若何
    人向窗前看明月
    月從窗隙看詩家
    `
  };
  contentEncode(content);
  const tx: Transaction = {
    ...data.transaction,
    memo: Buffer.alloc(0),
    params: {
      keys: [],
      content: contentEncode(content)
    },
    signature: Buffer.alloc(64, 0)
  }
  sign(tx, 'SAOPUWKOSPA5BILCP2YTDW2KRWMCBYLBC2EPVXGLUQSPPZV4222CXLET');
  await axios.post('http://localhost:4000/commit/v1/rpc_commit', {
    transaction: encode(tx).toString('base64')
  });
  res.setHeader('Content-Type', 'application/json');
  res.send({tx: `0x${encode(tx).toString('hex')}`});
});

app.get('/ex/follow', async (req, res) => {
  const { data } = await axios.get('http://localhost:4000/commit/v1/create/GB73OPHUZC3RSDEU2LYV5T7MEAN2Q26HYQPDYIENGNBUHW5CXAQ6UJOO/update_account');
  const tx: Transaction = {
    ...data.transaction,
    memo: Buffer.alloc(0),
    params: {
      key: 'followings',
      value: followingEncode(['GBUHOVI2MHLTRUGI234VMDNEJDO3DNOX37XOIQFUKBYD5QHAUYG2ETYP', 'GAO4J5RXQHUVVONBDQZSRTBC42E3EIK66WZA5ZSGKMFCS6UNYMZSIDBI'])
    },
    signature: Buffer.alloc(64, 0)
  };
  
  sign(tx, 'SAOPUWKOSPA5BILCP2YTDW2KRWMCBYLBC2EPVXGLUQSPPZV4222CXLET');
  await axios.get(`http://localhost:4000/commit/v1/commit/0x${encode(tx).toString('hex')}`);
  res.setHeader('Content-Type', 'application/json');
  res.send({tx: `0x${encode(tx).toString('hex')}`});
})

app.get('/ex/change_name', async (req, res) => {
  const { data } = await axios.get('http://localhost:4000/commit/v1/create/GB73OPHUZC3RSDEU2LYV5T7MEAN2Q26HYQPDYIENGNBUHW5CXAQ6UJOO/update_account');
  const tx: Transaction = {
    ...data.transaction,
    memo: Buffer.alloc(0),
    params: {
      key: 'name',
      value: Buffer.from('📖 Nguyễn Du REBORN 📖')
    },
    signature: Buffer.alloc(64, 0)
  };

  sign(tx, 'SAOPUWKOSPA5BILCP2YTDW2KRWMCBYLBC2EPVXGLUQSPPZV4222CXLET');
  await axios.get(`http://localhost:4000/commit/v1/commit/0x${encode(tx).toString('hex')}`);
  res.setHeader('Content-Type', 'application/json');
  res.send({tx: `0x${encode(tx).toString('hex')}`});
})

app.get('/ex/avatar', async (req, res) => {
  const { data } = await axios.get('http://localhost:4000/commit/v1/create/GBUHOVI2MHLTRUGI234VMDNEJDO3DNOX37XOIQFUKBYD5QHAUYG2ETYP/update_account');
  const image = readFileSync(__dirname + '/a3.jpg');
  const tx: Transaction = {
    ...data.transaction,
    memo: Buffer.alloc(0),
    params: {
      key: 'picture',
      value: image
    },
    signature: Buffer.alloc(64, 0)
  };
  sign(tx, 'SCDYCOAJWXMCWNBMADJ2QN4SXZKXLQRDWMZHFAFRFSRH3NBD3DB4ON4M');
  await axios.post('http://localhost:4000/commit/v1/rpc_commit', {
    transaction: encode(tx).toString('base64')
  });
  res.setHeader('Content-Type', 'application/json');
  res.send({tx: `0x${encode(tx).toString('hex')}`});
})

app.get('/ex/interact', async (req, res) => {
  const { data } = await axios('http://localhost:4000/commit/v1/create/GB73OPHUZC3RSDEU2LYV5T7MEAN2Q26HYQPDYIENGNBUHW5CXAQ6UJOO/interact');
  // const reactContent = {
  //   type: 1,
  //   text: 'Comment 2nd'
  // }

  const reactContent = {
    type: 2,
    reaction: 2
  }

  const tx: Transaction = {
    ...data.transaction,
    memo: Buffer.alloc(0),
    params: {
      object: '146D4FFABAED96996486F20B47F526E099915CC9C960405FEE9F4F26B4918362',
      content: contentEncode(reactContent)
    },
    signature: Buffer.alloc(64, 0)
  }
  sign(tx, 'SAOPUWKOSPA5BILCP2YTDW2KRWMCBYLBC2EPVXGLUQSPPZV4222CXLET');
  await axios.post('http://localhost:4000/commit/v1/rpc_commit', {
    transaction: encode(tx).toString('base64')
  });
  res.setHeader('Content-Type', 'application/json');
  res.send({tx: `0x${encode(tx).toString('hex')}`});
})

app.use('/database/v1', databaseAPI );
app.use('/blockchain/v1', blockchainAPI);
app.use('/commit/v1',  commitAPI);

app.listen(port, (err) => {
  if (err) {
    console.log(err);
  }
  console.log(colors.green(`Server started on port: ${port}`));
});


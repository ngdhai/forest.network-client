import { groupBy, sortBy, flatten, uniqBy, findIndex, propEq, equals, add, isEmpty, subtract, sort, reverse, compose, map, update, reject, prop, merge } from 'ramda';
import {
  TransactionModel,
  UserModel,
  IUser,
  IPost,
  ITransaction,
  IFollowing,
  FollowingModel,
  PostModel,
} from '../db/models';
import { getRange } from './utils';
import { Block } from '../types';
import { getBlocks, getBlockChainHeight } from './blockchain-api';
import { encode } from '../../lib/tx';
import colors from 'colors/safe';
import mongoose from 'mongoose';
import { followingDecode, contentDecode } from '../encoder';
import moment = require('moment');
import { PERIOD, SYSTEM_EVERGY, MAX_CEL } from '../constants/forest-network';

let fetchedHeight = 0;
let interval: any;

const getSequence = async (publicKey: string) => {
  const madedTransaction = await TransactionModel.find({
    account: publicKey
  }).exec();
  return madedTransaction.length;
};

const getAllTransaction = async (publicKey: string) => {
  const madedTransactions = await TransactionModel.find({
    account: publicKey
  }).exec();
  const targetTransactions = await TransactionModel.find({
    'params.address': { $exists: true, $eq: publicKey }
  }).exec();
  return [...madedTransactions, ...targetTransactions];
};

const getTransaction = async (publicKey: string) => {
  const transactions = await TransactionModel.find({
    account: publicKey
  }).exec();
  return sortBy(tran => tran.time, transactions);
};

const getBalance = async (publicKey: string) => {
  const user = await UserModel.findOne({ account: publicKey }).exec();
  return user.balance;
};

const getUser = async (publicKey: string) => {
  const user = await UserModel.findOne({ account: publicKey }).exec();
  return user;
};

const getPosts = async (publicKey: string) => {
  const posts = await TransactionModel.find({ account: publicKey, operation: 'post' }).exec();
  return reverse(sortBy(post => post.height, posts));
};

const getFollowings = async (publicKey: string) => {
  const data = await FollowingModel.findOne( { account: publicKey }).exec();
  return data;
}

const getFollowingsDetail = async (publicKey: string) => {
  const { addresses } = await FollowingModel.findOne({ account: publicKey }).exec();
  const promises = await addresses.map(async address => {
    const user = await UserModel.findOne({ account: address }).exec()
    return user;
  });
  const users = await Promise.all(promises);
  return users;
}

const fetch = async (currentHeight: number, from = 1) => {
  let users = Array<IUser>();
  let posts = Array<IPost>();
  let store = Array<Block>();
  const followings = Array<IFollowing>();

  for (const range of getRange(currentHeight, from)) {
    const { from, to } = range;
    console.log(colors.cyan(`Fetch block range: ${from} --> ${to}`));
    const blocks = flatten<Block>(await getBlocks(from, to));
    store.push(...blocks.filter(block => !!block.txs));
  }
  fetchedHeight = add(currentHeight, 1);

  console.log(store.length);
  store.map(block => {
    const { time, height, dataHash } = block;
    const txs = uniqBy(tx => tx.account, block.txs);
    txs.map(tx => {
      switch (tx.operation) {
        case 'create_account': {
          const { params } = tx;
          const { address } = params;
          users.push({
            account: address,
            height,
            time,
            balance: 0
          } as IUser);
          break;
        }
        case 'update_account': {
          const { params, account } = tx;
          const { key, value } = params;
          switch (key) {
            case 'name': {
              const index = findIndex(propEq('account', account), users);
              users[index].displayName = value.toString('utf8');
              break;
            }
            case 'picture': {
              const index = findIndex(propEq('account', account), users);
              users[index].image = value;
              break;
            }
            case 'followings': {
              const index = findIndex(propEq('account', account), followings);
              if (!equals(index, -1)) {
                followings[index].addresses = followingDecode(value);
              } else {
                followings.push({
                  account,
                  addresses: followingDecode(value)
                } as IFollowing)
              }
            }
          }
          break;
        }
        case 'payment': {
          const { params, account } = tx;
          const { address, amount } = params;
          const fromIndex = findIndex(propEq('account', account), users);
          if (!equals(fromIndex, -1)) {
            users[fromIndex].balance -= amount;
          }
          const toIndex = findIndex(propEq('account', address), users);
          if (!equals(toIndex, -1)) {
            users[toIndex].balance += amount;
          }
         
          break;
        }
        case 'post': {
          const { params, account } = tx;
          const { keys, content } = params;
          posts.push({
            account,
            height,
            time,
            content,
            keys,
            dataHash
          } as IPost);
          break;
        }
        case 'interact': {
          const { params, account } = tx;
          const { object, content } = params;
          const userIndex = findIndex(propEq('account', account), users);
          const displayName = users[userIndex].displayName || account; 
          const decoded = contentDecode(content);
          if (decoded !== undefined) {
            const index = findIndex(propEq('dataHash', object), store);
            if (index !== -1) {
              switch(decoded.type) {
                case 1: {
                  store[index].reacts = store[index].reacts ? [...store[index].reacts, { account, ...decoded, time, displayName }] : [{account, ...decoded, time}]
                  break;
                }
                case 2: {
                  if (!store[index].reacts) {
                    store[index].reacts = [{account, ...decoded, time, displayName }];
                  } else {
                    const reactions = store[index].reacts.filter(reaction => reaction.type === 2);
                    if (isEmpty(reactions)) {
                      store[index].reacts = [...store[index].reacts, { account, ...decoded, time, displayName }];
                    } else {
                      const replaceIndex = findIndex(propEq('account', account), reactions);
                      const temp = replaceIndex === -1 ? [...reactions, {account, ...decoded, time, displayName}] : update(replaceIndex, { account, ...decoded, time, displayName }, reactions);
                      store[index].reacts = [...reject(react => react.type === 2, store[index].reacts), ...temp];
                    }
                  }
                  break;
                }
              }
            }
          }
        } 
      }
    });
  });
  mongoose.connect(
    'mongodb://localhost:27017/test',
    { useNewUrlParser: true }
  );

  followings.map(following => {
    const model = new FollowingModel(following)
    model.save();
  });

  users.map(user => {
    const model = new UserModel({
      ...user,
      displayName: user.displayName || user.account
    });
    model.save();
  });

  posts.map(post => {
    const model = new PostModel(post);
    model.save();
  });

  store.map(block => {
    const { height, time, dataHash, reacts } = block;
    const txs = uniqBy(tx => tx.account, block.txs);
    txs.map(tx => {
      const model = new TransactionModel({
        account: tx.account,
        height,
        memo: tx.memo,
        operation: tx.operation,
        params: tx.params,
        sequence: tx.sequence,
        time,
        size: encode(tx).byteLength,
        dataHash,
        reacts: reacts ? reacts : []
      } as ITransaction);
      model.save();
    });
  });

  return 'Fetching done';
};

const refetch = () => {
  interval = setInterval(async () => {
    const currentHeight = await getBlockChainHeight();
    if (currentHeight < fetchedHeight) return;

    let store = Array<Block>();
    let users = Array<IUser>();
    let posts = Array<IPost>();

    for (const range of getRange(currentHeight, fetchedHeight)) {
      const { from, to } = range;
      console.log(colors.cyan(`Fetch block range: ${from} --> ${to}`));
      const blocks = flatten<Block>(await getBlocks(from, to));
      store.push(...blocks.filter(block => !!block.txs));
    }
    fetchedHeight = currentHeight;

    if (!isEmpty(store)) {
      store.map(block => {
        const { time, height, dataHash } = block;
        const txs = uniqBy(tx => tx.account, block.txs);
        txs.map(tx => {
          switch (tx.operation) {
            case 'create_account': {
              const { params } = tx;
              const { address } = params;
              users.push({
                account: address,
                height,
                time,
                balance: 0,
              } as IUser);
              break;
            }
            case 'update_account': {
              const { params, account } = tx;
              const { key, value } = params;
              switch (key) {
                case 'name': {
                  UserModel.findOneAndUpdate({ account: account}, {displayName: value.toString('utf8')}).exec();
                  break;
                }
                case 'picture': {
                  UserModel.findOneAndUpdate({account: account}, {image: value}).exec();
                  break;
                }
                case 'followings': {
                  FollowingModel.findOneAndUpdate({ account }, { addresses: followingDecode(value)}, (err, doc) => {
                    if (err) {
                     
                      console.log(err);
                    } 
                    if (!doc) {
                       const newFollowing = new FollowingModel({
                        account,
                        addresses: followingDecode(value)
                      } as IFollowing)
                      newFollowing.save();
                    }
                  })
                }
              }
              break;
            }

            case 'payment': {
              const { params, account } = tx;
              const { address, amount } = params;
              UserModel.findOne({ account: account }).then(user => {
                if (user) {
                  user.balance = subtract(user.balance, amount);
                  user.save();
                }
              })
              UserModel.findOne({ account: address}).then(user => {
                if (user) {
                  user.balance = add(user.balance, amount);
                  user.save();
                }
              })
              break;
            }

            case 'post': {
              const { params, account } = tx;
              const { keys, content } = params;
              posts.push({
                account,
                height,
                time,
                content,
                keys,
                dataHash
              } as IPost);
              break;
            }

            case 'interact': {
              const { params, account } = tx;
              const { object, content } = params;
              
              try {
                const decoded = contentDecode(content);

                if (decoded) {
                  UserModel.findOne({ account: account }, (err, user) => {
                    if (user) {
                      const displayName = user.displayName || user.account;
                      TransactionModel.findOne({ dataHash: object }, (err, doc) => {
                        if (doc) {
                          switch(decoded.type) {
                            case 1: {
                              doc.reacts.push({ account, ...decoded, time, displayName });
                              break;
                            }
                            case 2: {
                              const reactions = doc.reacts.filter(react => react.type === 2);
                              if (isEmpty(reactions)) {
                                doc.reacts = [...doc.reacts, { account, ...decoded, time, displayName }];
                              } else {
                                const replaceIndex = findIndex(propEq('account', account), reactions);
                                const temp = replaceIndex === -1 ? [...reactions, {account, ...decoded, time, displayName}] : update(replaceIndex, { account, ...decoded, time, displayName }, reactions);
                                doc.reacts = [...reject(react => react.type === 2, doc.reacts), ...temp];
                              }
                              break;
                            }
                          }
                          doc.save();
                        }
                      })
                    }
                  })
                   
                }
               
              } catch (err) {
                console.log(err);
              }
            }
          }
        });
      });
      console.log(store);
    }

    mongoose.connect(
      'mongodb://localhost:27017/test',
      { useNewUrlParser: true }
    );
  
    users.map(user => {
      const model = new UserModel({
        ...user,
        displayName: user.displayName || user.account
      });
      model.save();
    });
  
    posts.map(post => {
      const model = new PostModel(post);
      model.save();
    });
  
    store.map(block => {
      const { height, time, dataHash, reacts } = block;
      const txs = uniqBy(tx => tx.account, block.txs);
      txs.map(tx => {
        const model = new TransactionModel({
          account: tx.account,
          height,
          memo: tx.memo,
          operation: tx.operation,
          params: tx.params,
          sequence: tx.sequence,
          time,
          size: encode(tx).byteLength,
          dataHash,
          reacts: reacts ? reacts : []
        } as ITransaction);
        model.save();
      });
    });

  }, 1 * 1000);
};

const stopFetch = () => {
  clearInterval(interval);
};

const getEnergy = async (publicKey: string) => {
  try {
    const transactions = await getTransaction(publicKey);
    const data = transactions.map(tran => ({
      time: tran.time,
      rawEnergy: tran.size
    }));
    const usedEnergy = data.reduce((tran, elem) => {
      const diff = moment(elem.time).unix() - moment(tran.time).unix();
      tran.time = elem.time;
      tran.rawEnergy = Math.ceil(tran.rawEnergy * Math.max(0, (PERIOD - diff)/PERIOD) + elem.rawEnergy);
      return tran;
    });
  
    const balance = await getBalance(publicKey);
    const diff = moment(Date.now()).unix() - moment(usedEnergy.time).unix();
    const energy = Math.ceil(balance * (SYSTEM_EVERGY / MAX_CEL) - usedEnergy.rawEnergy * Math.max(0, (PERIOD - diff)/PERIOD));
    return {
      energy,
      time: usedEnergy.time
    }
  } catch(err) {
    return {
      energy: 0,
      time: Date.now()
    }
  }
  
}

const getFollowers = async (publicKey: string) => {
  const datas = await FollowingModel.find({ addresses: publicKey }).exec();
  const followerIds = datas.map(data => {
    return data.account
  });
  const promises = followerIds.map(async id => {
    return await UserModel.findOne({ account: id}).exec();
  });
  return await Promise.all(promises);
}

const getFollowingCount = async (publicKey: string) => {
  const data = await FollowingModel.findOne({ account: publicKey}).exec();
  return data ? data.addresses.length : 0;
}

const getAllUserSimple = async () => {
  const data = await UserModel.find().exec();
  return data.map(({ account, time, displayName }) => ({
    publicKey: account,
    time,
    displayName
  }));
}

const getNewFeed = async (publicKey: string) => {
  try {
    const user = await FollowingModel.findOne({ account: publicKey }).exec();
    const users = compose(
      groupBy(prop('account')),
      map(user=> user['_doc'])
    )(await UserModel.find({ account: { $in:  user.addresses }}).exec());
    const feeds = map(res => res['_doc'], await TransactionModel.find( { account: { $in: user.addresses }, operation: 'post' }));
    const result = map(feed => merge(users[feed.account][0], feed), feeds);
    return reverse(sortBy(res => res.height, result));
  } catch (err) {
    return [];
  }
}

const getPayment = async (publicKey: string) => {
  try {
    const payments = await TransactionModel.find({ account: publicKey, operation: 'payment' }).exec();
    return reverse(sortBy(payment => payment.height, payments));
  } catch (err) {
    return []
  }
}


export {
  getSequence,
  getAllTransaction,
  getTransaction,
  getBalance,
  getUser,
  refetch,
  fetch,
  stopFetch,
  getPosts,
  getFollowings,
  getEnergy,
  getFollowers,
  getFollowingsDetail,
  getFollowingCount,
  getAllUserSimple,
  getNewFeed,
  getPayment
};

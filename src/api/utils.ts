import { BlockInfo } from '../types';
import R, { filter } from 'ramda';
import { Operation } from '../types';

const createAccountTransactions = R.filter((block: BlockInfo) => block.txs && R.equals<Operation>(block.txs[0].operation, 'create_account'));

const paymentTransactions = R.filter((block: BlockInfo) => block.txs && R.equals<Operation>(block.txs[0].operation, 'payment'));

const postTransactions = R.filter((block: BlockInfo) => block.txs && R.equals<Operation>(block.txs[0].operation, 'post'));

const updateAccountTransactions = R.filter((block: BlockInfo) => block.txs && R.equals<Operation>(block.txs[0].operation, 'update_account'));

const blockHasTransaction = R.filter((block: BlockInfo) => !R.equals(block.txs, null));

const getRange = (height: number, start = 1, distance = 100) => {
  let diff = start + distance;
  let from = start;
  const ranges = Array<{from: number, to: number}>();
  while(diff < height) {
    ranges.push({ from, to: diff });
    from = diff;
    diff += distance;
  }

  ranges.push({from: +from, to: +height })
  return ranges;
};

const applyValue = (value: string | number) => (str: string) => `${str}${value}`;

const endpoint = (ep: string) => (str: string) => `${str}${ep}`;
const query = (param: string) => (str: string) => `${str}?${param}`;
const more = (param: string) => (str: string) => `${str}&${param}`;

export {
  getRange,
  createAccountTransactions,
  paymentTransactions,
  postTransactions,
  updateAccountTransactions,
  blockHasTransaction,
  applyValue,
  endpoint,
  query,
  more
}
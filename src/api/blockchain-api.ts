import { 
  compose, 
  pipe, 
  range, 
  flatten, 
  map, 
  filter, 
  equals, 
  isEmpty, 
  reduce 
} from 'ramda';
import { 
  API_URL, 
  TX_SEARCH, 
  PER_PAGE, QUERY, 
  PAGE, BLOCK, 
  HEIGHT,
  ABCI_INFO, 
  BROADCAST_TX_COMMIT, 
  QTX, 
  API_URL_HTTP,
  SYSTEM_EVERGY,
  MAX_CEL
} from '../constants/forest-network';
import { decode, encode } from '../../lib/tx';
import {
  getRange,
  applyValue,
  endpoint,
  more,
  query,
} from './utils';
import moment = require('moment');
import axios from 'axios';
import { Block, Transaction } from '../types';

const getBlockChainHeight = async () => {
  const { data } = await pipe(
    endpoint(ABCI_INFO),
    axios.get
  )(API_URL);
  return +data.result.response.last_block_height;
}

const getBlock = async (height: number) => {
  const { data } = await pipe(
    endpoint(BLOCK),
    query(HEIGHT),
    applyValue(height),
    axios.get
  )(API_URL);

  const { block } = data.result;
  return {
    height: +block.header.height,
    numTxs: +block.header.num_txs,
    totalTxs: +block.header.total_txs,
    time: new Date(block.header.time),
    dataHash: block.header.data_hash,
    txs: block.data.txs && block.data.txs.map(trans => decode(Buffer.from(trans, 'base64')))
  } as Block
}

const getBlocks =async (from: number, to: number) => {
  const blockIds = range(from, to);
  const promises = blockIds.map(async id => {
    try {
      const block = await getBlock(id);
      return block;
    } catch (err) {
      throw err;
    }
  });
  return await Promise.all(promises);
}

const getTotalTransaction = async (publicKey: string) => {
  const { data } = await pipe(
    endpoint(TX_SEARCH),
    query(QUERY),
    applyValue(`"account='${publicKey}'"`),
    axios.get
  )(API_URL);


  return  +data.result.total_count;
}

const getAllTransaction = async (publicKey: string) => {
  const totalCount = await getTotalTransaction(publicKey);

  const ranges = getRange(totalCount, 1, 20);
  const pages = range(1, ranges.length + 1);
  const promises = pages.map(async page => {
    const { data } = await pipe(
      endpoint(TX_SEARCH),
      query(QUERY),
      applyValue(`"account='${publicKey}'"`),
      more(PAGE),
      applyValue(page),
      more(PER_PAGE),
      applyValue(20),
      axios.get
    )(API_URL);
    return data.result.txs;
  });

  const transactions = await Promise.all(promises);
  return flatten(transactions);
}

const getSequence = async (publicKey: string) => {
  const encodedDatas = map(
    (tran: any) => tran.tx, 
    await getAllTransaction(publicKey)
  );
  const transactions: Transaction[] = encodedDatas.map(data => decode(Buffer.from(data, 'base64')));
  const madedTransactions : Transaction[] = transactions.filter(tran => equals(tran.account, publicKey));
  
  return isEmpty(madedTransactions) ? 
    0 : compose(
      sequences => Math.max(...sequences),
      map((tran: Transaction) => tran.sequence)
    )(madedTransactions);
}

const getBalance = async (publicKey: string) => {
  const encodedDatas = map(
    (tran: any) => tran.tx, 
    await getAllTransaction(publicKey)
  );
  const transactions: Transaction[] = encodedDatas.map(data => decode(Buffer.from(data, 'base64')));
  const totalReceive = compose(
    reduce<number, number>((acc, cur) => acc + cur, 0),
    map((tran: Transaction) => tran.params.amount)
  )(transactions.filter(tran => tran.operation === 'payment' && tran.params.address === publicKey));

  const totalTransfer = compose(
    reduce<number, number>((acc, cur) => acc + cur, 0),
    map((tran: Transaction) => tran.params.amount)
  )(transactions.filter(tran => tran.operation === 'payment' && tran.account === publicKey));
  
  return {
    totalReceive,
    totalTransfer,
    balance: totalReceive - totalTransfer
  };
}

const commitTransaction = async (tx: string) => {
  const { data } = await pipe(
    endpoint(BROADCAST_TX_COMMIT),
    query(QTX),
    applyValue(tx),
    axios.get
  )(API_URL_HTTP);
  return {
    ...data
  }
}


const getEnergyRequireForBlock = async (height: number, publicKey: string) => {
  const block = await getBlock(height);
  const tx: Transaction = {
    ...block.txs[0]
  }
  return tx.account === publicKey ? 
  { rawEnergy:  encode(tx).byteLength,
    time: block.time } : {rawEnergy: 0,time: 0}
}

const getEnergy = async (publicKey: string) => {
  const blockIds = compose(
    map((tran: any) => tran.height), 
    filter((tran: any) => {
      const tx: Transaction = decode(Buffer.from(tran.tx, 'base64'));
      return equals(tx.account, publicKey);
  }))(await getAllTransaction(publicKey));

  const promises = blockIds.map(async id => {
    const data = await getEnergyRequireForBlock(id, publicKey);
    return data;
  });
  const data = await Promise.all(promises);

  const usedEnergy = data.reduce((acc, cur) => {
    const diff = moment(cur.time).unix() - moment(acc.time).unix();
    acc.time = cur.time;
    acc.rawEnergy = Math.ceil(acc.rawEnergy * Math.max(0, (86400 - diff)/86400) + cur.rawEnergy);
    return acc;
  });
  const { balance } = await getBalance(publicKey);
  const diff = moment(Date.now()).unix() - moment(usedEnergy.time).unix()
  const energy = Math.ceil(balance * (SYSTEM_EVERGY / MAX_CEL) - usedEnergy.rawEnergy * Math.max(0, (86400 - diff)/86400));

  return {
    energy,
    time: usedEnergy.time
  }
}

export {
  getBlockChainHeight,
  getBlock,
  getBlocks,
  getSequence,
  getTotalTransaction,
  getAllTransaction,
  getBalance,
  commitTransaction,
  getEnergy,
  getEnergyRequireForBlock
}
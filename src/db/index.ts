import mongoose, { Connection } from 'mongoose';
import { IUser, UserModel } from './models';
import colors from 'colors/safe';

export default class Database {
  connection: Connection;

  constructor(public url: string) {}

  connect = async () => {
    await mongoose.connect(this.url, { useNewUrlParser: true });
    this.connection = mongoose.connection;
    this.connection.on('error', console.error.bind(console, colors.bgRed('Connection Error: ')));
    return this.url;
  }

  addUsers = async (users: IUser[]) => {
    const promises = users.map(async user => {
      try {
        const newUser = new UserModel({
          account: user.account,
          height: user.height,
          time: user.time,
        } as IUser)
        return await newUser.save();
      } catch (err) {
        throw err;
      }
    });

    const addedUsers = await Promise.all(promises);
    return addedUsers;
  }

  close = () => {
    this.connection.close(err => {
      err ? console.log(err) : null;
    });
  }
}
import { Document, model, Schema, SchemaDefinition } from 'mongoose';

export interface IUser {
  account: string;
  time?: Date;
  height: number;
  displayName?: string;
  image?: Buffer;
  balance?: number
};

interface UserModel extends Document, IUser {
  getDisplayName(): string;
};

const definition: SchemaDefinition = {
  account: { type: String, required: true, unique: true },
  time: { type: Date },
  height: { type: Number, required: true },
  displayName: { type: String },
  image: { type: Buffer },
  balance: { type: Number }
};

const userSchema = new Schema(definition);

userSchema.methods.getDisplayName = function() {
  const user = this as IUser;
  return user.displayName || user.account;
};

export default model<UserModel>('User', userSchema);
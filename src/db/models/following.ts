import { Document, model, Schema, SchemaDefinition } from 'mongoose';

export interface IFollowing {
  account: string;
  addresses: Array<string>;
};

interface FollowingModel extends Document, IFollowing {

};

const definition: SchemaDefinition = {
  account: { type: String, required: true, unique: true },
  addresses: { type: [String] }
};

const followingSchema = new Schema(definition);

export default model<FollowingModel>('Following', followingSchema);


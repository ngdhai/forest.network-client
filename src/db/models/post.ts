import { Document, model, Schema, SchemaDefinition } from 'mongoose';

export interface IPost {
  account: string,
  time?: Date,
  height: number,
  content: Buffer,
  keys: Buffer[],
  dataHash: string
}

interface PostModel extends Document, IPost {}

const definition: SchemaDefinition = {
  account: { type: String, required: true },
  time: { type: Date },
  height: { type: Number, required: true },
  content: { type: Buffer },
  keys: { type: [Buffer] },
  dataHash: { type: String }
};

const postSchema = new Schema(definition);

export default model<PostModel>('Post', postSchema);
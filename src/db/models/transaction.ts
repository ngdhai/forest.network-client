import { Document, model, Schema, SchemaDefinition } from 'mongoose';

export interface ITransaction {
  sequence?: number,
  account?: string,
  memo?: Buffer,
  operation?: string,
  params?: any,
  time?: Date,
  height?: number,
  size?: number,
  dataHash?: string,
  reacts: any[]
}

interface TransactionModel extends Document, ITransaction {

}

const definition: SchemaDefinition = {
  sequence: { type: Number, required: true },
  account: { type: String },
  memo: { type: Buffer },
  operation: { type: String },
  params: { type: {} },
  time: { type: Date },
  height: { type: Number },
  size: {type: Number},
  dataHash: { type: String },
  reacts: { type: [{}] }
}

const transactionSchema = new Schema(definition);

export default model<TransactionModel>('Transaction', transactionSchema);

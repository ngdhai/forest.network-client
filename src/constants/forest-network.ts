const API_URL_WSS = 'wss://komodo.forest.network:443';
const API_URL_HTTP = 'https://dragonfly.forest.network:443';
const API_URL = 'http://localhost:26657';
const ABCI_INFO = '/abci_info';
const BLOCK = '/block';
const BROADCAST_TX_COMMIT = '/broadcast_tx_commit';
const TX = '/tx';
const TX_SEARCH = '/tx_search';
const QUERY_HEIGHT = '?height=';
const QUERY_HASH = '?hash=';
const QUERY_QUERY = '?query=';
const QUERY_TX = '?tx=';
const QUERY_PAGE = '?page=';
const QUERY_PER_PAGE = '?per_page=';

const HEIGHT = 'height=';
const HASH = 'hash=';
const QUERY = 'query=';
const QTX = 'tx=';
const PAGE = 'page=';
const PER_PAGE = 'per_page=';
const RESERVE_RATIO = 1;
const PERIOD = 86400;
const MAX_SIZE = 22020096;
const SYSTEM_EVERGY = RESERVE_RATIO * PERIOD * MAX_SIZE;
const MAX_CEL = Number.MAX_SAFE_INTEGER;
const PASSWORD = 'b2debb05e41b6171beda1631712b6f352d3bd3c9ad7a0af61d153e0f24ec8288'


export {
  API_URL_WSS,
  API_URL_HTTP,
  API_URL,
  ABCI_INFO,
  BLOCK,
  BROADCAST_TX_COMMIT,
  TX,
  TX_SEARCH,
  QUERY_HASH,
  QUERY_HEIGHT,
  QUERY_QUERY,
  QUERY_TX,
  QUERY_PAGE,
  QUERY_PER_PAGE,
  HASH,
  QUERY,
  QTX,
  PAGE,
  PER_PAGE,
  HEIGHT,
  SYSTEM_EVERGY,
  MAX_CEL,
  PASSWORD,
  PERIOD
}